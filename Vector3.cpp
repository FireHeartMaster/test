//#include "Vector3.h"
class Vector3{
	public:
		float x, y, z;

		Vector3(){
			x = 0.0;
			y = 0.0;
			z = 0.0;
		}

		Vector3(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {
		}


		Vector3 operator+(const Vector3& vector) {
			Vector3 sum = Vector3(this->x + vector.x, this->y + vector.y, this->z + vector.z);

			return sum;
		}
};

